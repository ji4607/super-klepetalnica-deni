/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
 var nadimki = [[],[]];
 var counter = 0;

 
 
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


///////////////////////////////////
  //DODAJANJE SLIK//
  ///////////////////////////////////
function dodajSliko(sporocilo) {
  var besede = sporocilo.split(' ');
  //alert("ali je to prav " + besede[0]);
  var i;
  for (i = 0; i < besede.length; i++) {
    if (besede[i].includes('https://sandbox.lavbic.net/teaching/OIS/gradivo/')) {}
    else if(besede[i].includes('http') || besede[i].includes('.jpg') || besede[i].includes('.gif') || besede[i].includes('.png') ){
        besede[i] = "<img src=" + '"'+besede[i] + '" ' + "width="+'"200px' +'" ' + "style=" + '"' + "margin-left:20px;" + '"' + " >";
    //width="200px"   style="margin-left:20px;">
  }
    
    
  }
  if ((besede[0].includes('http') || besede[0].includes('.jpg') || besede[0].includes('.png') || besede[0].includes('.gif'))){
    
  }
  else {
    if(besede[0].includes(':')){
    besede[0] = besede[0].bold();
    }
  }
  //besede[0] = besede[0].bold();
  sporocilo = besede.join(' ');
  
  
  return sporocilo;
  
}

/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
               
    if ((sporocilo.includes('http') && sporocilo.includes('.jpg')) ||( sporocilo.includes('http') && sporocilo.includes('.png') )||( sporocilo.includes('http') && sporocilo.includes('.gif')  )) {
      return divElementHtmlTekst(dodajSliko(sporocilo));
    }
   else {
      return divElementHtmlTekst(sporocilo);
    }
    
  } else if((sporocilo.includes('http') && sporocilo.includes('.jpg')) ||( sporocilo.includes('http') && sporocilo.includes('.png') )||( sporocilo.includes('http') && sporocilo.includes('.gif')  )) {
    
    return divElementHtmlTekst(dodajSliko(sporocilo));
    
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  //sporocilo = dodajSliko(sporocilo);
  var sistemskoSporocilo;
  
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    //alert("zaznan ukaz / " );
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
//<<<<<<< HEAD
      sistemskoSporocilo = dodajSliko(sistemskoSporocilo);
      //$('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
//=======
      //alert("v prvwem if stavku" );
      if (sistemskoSporocilo.includes('&%&')) {
        //alert("v drugemn if stavku" );
        //sistemskoSporocilo = sistemskoSporocilo.replace(/\/p/, '');
        sistemskoSporocilo = sistemskoSporocilo.replace('&%& ', '');
        var imena = sistemskoSporocilo.split(',');
        var zamenjano = false;
        
        for(var i = 0; i< nadimki[0].length;i++) {
          
          if(imena[0].trim() == nadimki[0][i].trim()) {
            
            nadimki[1][i] = imena [1];
            zamenjano = true;
            break;
          }
        }
      
        if (zamenjano == false) {
          nadimki [0][counter] = imena[0].replace(' ','');
          nadimki [1][counter] = imena [1];
          counter = counter + 1;
        }
      
      }
      
      else {
        //alert("v elsuu" );
        var bolean1 = false;
        //alert(" 1." );
        if(sistemskoSporocilo.includes('(zasebno za')) {
          //alert(" 2." );
          var zamenjajIme = sistemskoSporocilo.split(' ');
          //alert(" 3. - " + zamenjajIme[0] + ", " + zamenjajIme[1] + ","+ zamenjajIme[2] );
          zamenjajIme[2] = zamenjajIme[2].replace('):','');
          
          //alert(" 4. - " + zamenjajIme[0] + ", " + zamenjajIme[1]);
          for (var i = 0; i< nadimki[0].length ; i++) {
            //alert(" 5. " + zamenjajIme[2] );
            
            if(nadimki[0][i].trim() == zamenjajIme[2].trim()) {
              //alert(" 6." );
                zamenjajIme[2] = nadimki[1][i] + '):';
                //alert(" 7.-" + zamenjajIme[2] );
                sistemskoSporocilo = zamenjajIme.join(' ');
                //alert(" 8." );
                $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
                $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
              bolean1 = true;
              break;
            }
          }
          if(bolean1 == false) {
            $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
            $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
            
          }
          
          
        } else {
            $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
            $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
        }
      }
//>>>>>>> preimenovanje
    }
    
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }
//////dodajanje croll topppp
//// sdf frtgf ffdrgdf fdf f
  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  });
  
  
  //////////////////////////////////
  // Prikaži prejeto sporočilo
  ///////////////////////////////////
  
  socket.on('sporocilo', function (sporocilo) {
//<<<<<<< HEAD
    if((sporocilo.besedilo).includes('&#9756;')){
      console.log("pokec sporocilo-" + sporocilo.besedilo);
      var spremeniIme = sporocilo.besedilo.split(' ');
      for(var i = 0; i< nadimki[0].length;i++) {
        if(spremeniIme[0].trim() == nadimki[0][i].trim()){
          spremeniIme[0] = nadimki[1][i];
          
          break;
        }
      }
      sporocilo.besedilo = spremeniIme.join(' ');
      
      $('#sporocila').append(divElementHtmlTekst(sporocilo.besedilo.replace('&#9756;','').bold() + '&#9756;'));
      $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    }
    
  else {
    //alert("prejeto sporocilo-" + sporocilo.besedilo);
    //else {
    //var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    
    //$('#sporocila').append(novElement);
    //}
//=======
    // preveri ce se ujema z nadimkom...
      
      
      var novoSporocilo;
      //alert("msg-" + sporocilo.besedilo );
      var bul = false;
      var ime = sporocilo.besedilo.split(":");
      var imeX = sporocilo.besedilo.split(":");
      //alert("ime x " + imeX[0]);
      var ime3 = imeX[0].split(' ');
      var bull1 = false;
      //alert("dolzina je: "+ ime.length);
      //ime.shift();
      //var b = ime.join(":");
      //alert("new-" + b);
      for(var i = 0; i< nadimki[0].length; i++) {
        if (ime3[0].trim() == nadimki[0][i].trim() ) {
          ///
          //alert("najden nadimek");
          if(sporocilo.besedilo.includes("se je preimenoval") && !sporocilo.besedilo.includes(':')) {
            //alert(" v pogoju za preimenovanje");
            //ime.shift();
            //ime[0] = ime[0].replace('(','');
            //ime[0] = ime[0].replace(')','');
         // for(var i = 0; i< nadimki[0].length; i++) {
           // if(nadimki[0][i].trim() == ime[0].trim()) {
            
            var starNadimek = nadimki[1][i];
            console.log("ime3 predzadnji niz " + ime3[ime3.length-1]);
            ime3[ime3.length-1] = ime3[ime3.length-1].replace('.','');
            nadimki[1][i] = starNadimek.replace(nadimki[0][i].trim(),ime3[ime3.length-1].trim());
            console.log("nadimek " +  nadimki[1][i]);
            nadimki[0][i] = ime3[ime3.length-1].trim();
            ime3[ime3.length-1] = nadimki[1][i] + '.';
            ime3[0] = starNadimek;
            novoSporocilo = ime3.join(" ");
           // novoSporocilo = nadimki[1][i] + ":" + novoSporocilo;
            //var novElement = divElementEnostavniTekst(novoSporocilo);
            $('#sporocila').append(divElementHtmlTekst(novoSporocilo.bold()));
            $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
            bull1 = true;
            break;
            }
            
            ////
            else {
            //alert(" 1 -" + ime3[0] + " in " +  nadimki[0][i].trim());
            imeX[0] = imeX[0].replace(ime3[0].trim(),nadimki[1][i]);
            //imeX[0] = imeX[0].bold();
         console.log("DODATEK ime1[] nov! - " + imeX[0] + ", ime2 je " + ime3[0] + " in nadimek: " + nadimki[1][i] + " i je :" + i);
         //novoSporocilo = imeX.join(":"); //.bold();
         //alert("novo sporocilo" + novoSporocilo);
         var poke = imeX[0].bold();
         var slik = imeX[0];
            imeX.shift();
            poke = poke + ":" + imeX.join(':');
            //slik = slik + ":" + imeX.join(':');
            //alert("poke - " + poke);
            //alert("slik -" + slik);
            if(poke.includes('&#9756;')){
              //alert(" 2");
              //imeX[0] = imeX[0].bold();
              //novoSporocilo = imeX.join(":");
              
              $('#sporocila').append(divElementHtmlTekst(poke.replace('&#9756;','&#9756;')));
              $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
            }
            else {
              //alert("X-"+ novoSporocilo);
              //novoSporocilo = imeX.join(":");
              //$('#sporocila').append(divElementHtmlTekst(novoSporocilo));
              //alert("sporocilo.besedilo " + sporocilo.besedilo);
              var spor = dodajSmeske(imeX.join(':'))
              slik = slik + ":";
              slik = slik.bold();
              slik = slik + spor;
               $('#sporocila').append(divElementHtmlTekst(dodajSliko(slik)));
               $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
               
            }
            bull1 = true;
            break;
          }
            
            
          }  
          
          }
          
          ///
          ///
          ///
          
         //console.log("DODATEK izhod naj bi bil.. " + novoSporocilo);
         
        
        
      
      
      if (bull1 == false) {
      //alert(" 3");
      for(var i = 0; i< nadimki[0].length; i++) {
        if(nadimki[0][i].trim() == ime[0].trim()) {
          //alert(" pred includes :");
          //alert(" 4");
          
            ime.shift();
            novoSporocilo = ime.join(":");
            novoSporocilo = nadimki[1][i] + ":" + novoSporocilo;
            var novElement = divElementEnostavniTekst(novoSporocilo);
            bul = true;
            break;
          
        }
        
      }
      if (bul == false){
          //alert("brez zasebno...");
            var novElement = divElementEnostavniTekst(sporocilo.besedilo);
          
        
          
      }
      }
    $('#sporocila').append(novElement);
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    //dodan zaklepaj za else
  }
//>>>>>>> preimenovanje
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
        
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    
  /*  
<<<<<<< HEAD
    for (var i=0; i < uporabniki.length; i++) {
      $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
    }
    
    $('#seznam-uporabnikov div').click(function() {
      //else {
      var sistemskoSporocilo;
        sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"'+ $(this).text()+'"' +' "' +'&#9756;'+'"');
        if (sistemskoSporocilo) {
          $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
        //$('#sporocila').append(sistemskoSporocilo);
        }
      // else konec }
      
      
      
    });
======= */
    if (nadimki[0].length > 0) {
      
      var najdeno = false;
      for (var i=0; i < uporabniki.length; i++) {
        najdeno = false;
        for(var j = 0; j< nadimki[0].length ; j++) {
          if (nadimki[0][j].trim() == uporabniki[i].trim()) {
            $('#seznam-uporabnikov').append(divElementEnostavniTekst(nadimki[1][j]));
              najdeno = true;
            break;
          }
        }
        if (najdeno == false) {
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
        }
      
    }
      
      
    } else {
        for (var i=0; i < uporabniki.length; i++) {
          $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabniki[i]));
        }
    
      }
      // gor je staro
      //novo
    $('#seznam-uporabnikov div').click(function() {
      //else {
      var sistemskoSporocilo;
      //var oseba = $(this).text();
      console.log("v klick funkciji");
      if ($(this).text().includes("(")) {
        console.log("najden oklepaj");
        var oseba = $(this).text().split('(');
        oseba[1] = oseba[1].replace(')','');
        
         sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"'+ oseba[1]+'"' +' "' +'&#9756;'+'"');
        if (sistemskoSporocilo) {
          sistemskoSporocilo = sistemskoSporocilo.replace(oseba[1].trim(),$(this).text());
          $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
          $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
        
        }
        
      } else {
        console.log("else!");
          sistemskoSporocilo = klepetApp.procesirajUkaz('/zasebno ' + '"'+ $(this).text()+'"' +' "' +'&#9756;'+'"');
          if (sistemskoSporocilo) {
            $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
            $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
            //$('#sporocila').append(sistemskoSporocilo);
          }
        }
      // else konec }
      
      
      
    });
    
    
//>>>>>>> preimenovanje
    
    
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});
